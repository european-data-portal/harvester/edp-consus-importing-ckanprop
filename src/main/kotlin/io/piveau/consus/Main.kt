package io.piveau.consus

import io.vertx.core.Launcher

fun main(args: Array<String>) {
    Launcher.executeCommand("run", *(args.plus(CkanPropVerticle::class.java.name)))
}
