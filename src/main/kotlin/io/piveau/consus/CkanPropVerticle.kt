package io.piveau.consus

import io.piveau.pipe.PipeContext
import io.piveau.pipe.connector.PipeConnector
import io.vertx.core.CompositeFuture
import io.vertx.core.Future
import io.vertx.core.Promise
import io.vertx.core.eventbus.Message
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.client.WebClient
import io.vertx.ext.web.client.predicate.ResponsePredicate
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.awaitResult

class CkanPropVerticle : CoroutineVerticle() {

    private lateinit var client: WebClient

    override suspend fun start() {
        client = WebClient.create(vertx)

        vertx.eventBus().localConsumer(ADDRESS_PIPE, this::handlePipe)
        awaitResult<PipeConnector> { PipeConnector.create(vertx).onComplete(it) }.consumer(ADDRESS_PIPE)
    }

    private fun handlePipe(message: Message<PipeContext>): Unit = with(message.body()) {
        log.info("Import started.")

        val address = config.getString("address")

        client.getAbs("$address/public/ckan/dslist_eng.php").expect(ResponsePredicate.SC_OK).send {
            if (it.succeeded()) {
                val response = it.result().bodyAsJsonObject()
                if (response.getBoolean("success", false) == true) {
                    val datasets = response.getJsonArray("result", JsonArray())
                    val futures = mutableListOf<Future<Void>>()
                    datasets.forEachIndexed { index, elem ->
                        val datasetId = elem.toString().removeSuffix("%")
                        val dataInfo = json { obj(
                            "total" to datasets.size(),
                            "counter" to index,
                            "identifier" to elem.toString(),
                            "catalogue" to config.getString("catalogue")
                        ) }
                        futures.add(fetchDataset(datasetId, address, dataInfo, this))
                    }
                    CompositeFuture.join(futures as ArrayList<Future<Any>>).onComplete {
                        val delay = config.getLong("sendListDelay", 8000)
                        vertx.setTimer(delay.toLong()) {
                            val info = json {
                                obj(
                                    "content" to "identifierList",
                                    "catalogue" to config.getString("catalogue")
                                )
                            }
                            setResult(
                                datasets.encodePrettily(),
                                "application/json",
                                info
                            ).forward()
                            log.info("Import metadata finished")
                        }
                    }
                } else {
                    setFailure(response.getString("message", "No failure message available"))
                }
            } else {
                setFailure(it.cause())
            }
        }
    }

    private fun fetchDataset(datasetId: String, address: String, dataInfo: JsonObject, pipeContext: PipeContext) = Promise.promise<Void>().apply {
        client.getAbs("$address/public/ckan/dsdata_eng.php/$datasetId").expect(ResponsePredicate.SC_OK).send {
            if (it.succeeded()) {
                val response = it.result().bodyAsJsonObject()
                if (response.getBoolean("success", false) == true) {
                    val dataset = response.getJsonArray("result").getJsonObject(0)
                    pipeContext.setResult(dataset.encodePrettily(), it.result().getHeader("Content-Type"), dataInfo).forward()
                    pipeContext.log.info("Data imported: {}", dataInfo)
                } else {
                    pipeContext.log.error(response.getString("message", "No failure message available"))
                }
            } else {
                pipeContext.log.error("Fetching dataset $datasetId", it.cause())
            }
            complete()
        }
    }.future()

    companion object {
        const val ADDRESS_PIPE = "io.piveau.pipe.importing.ckanprop.queue"
    }

}